# Grouping

**Grouping** can be used on the [Round Aggregates](/endpoints/rounds-aggregate.md) endpoint to specify criteria on which to group rounds for the purpose of generating aggregates.

## Basic Usage

Grouping is achieved using the [groupBy](/endpoints/rounds-aggregate.md#groupBy) query parameter.

For fields that do not require any additional arguments, simply specify `?groupBy=fieldName`, substituting a valid field of the [Round](/datatypes/round.md) object. For example, `?groupBy=hostName`.

For fields that do require additional arguments, specify these inside parentheses, separated by commas. For example, `?groupBy=winTime(1, year)`.

## Returned Values

Each group will contain the aggregates requested, in addition to a field defining the group itself, depending on the field selected for grouping.

## Data Types

The following data types can be used for grouping:

### Exact Match

A field that is used directly in grouping, for example [hostName](/datatypes/round.md#hostName) or [winDayOfWeek](/datatypes/round.md#winDayOfWeek).
Rounds will be grouped based on having precisely the same value of the specified field.

No additional arguments are required.

*Examples:*
* `?groupBy=winDayOfWeek` - group rounds by the day of the week they were won on.
* `?groupBy=hostName` = group rounds by their host.

*Returns:*
The exact value of the field associated with each group. For example, `"hostName": "Provium"`

### Absolute Time

A field that represents a specific point in time, for example [winTime](/datatypes/round.md#winTime).
Rounds will be grouped based on falling into specific intervals determined by the arguments provided.

*Arguments:*
* interval (integer): The number of the given unit to use as an interval.
* unit (string): The unit to use for the interval. Allowed values are `hour`, `day`, `week`, `month`, `year`.

Note: For the purposes of grouping, weeks are taken as starting on Sundays.

*Examples:*
* `?groupBy=winTime(2, month)` - group rounds by their win time, in two-month intervals.
* `?groupBy=postTime(3, week)` - group rounds by their post time, in three-week intervals.

*Returns:*
A unix timestamp (seconds since midnight UTC on 1970-01-01) representing the beginning of the interval in which the group of rounds fall.
For example, `"postTime": 1640995200`

### Relative Time

A field that represents a point in time relative to something else, for example [winTimeOfDay](/datatypes/round.md#winTimeOfDay) (being relative to the start of the day the round was won on).
Rounds will be grouped based on falling into specific intervals determined by the arguments provided.

*Arguments:*
* interval (integer): The number of the given unit to use as an interval.
* unit (string): The unit to use for the interval. Allowed values are `minute`, `hour`.

*Examples:*
* `?groupBy=winTimeOfDay(1, hour)` - group rounds by the time of day they were won at, in one-hour intervals.
* `?groupBy=postTimeOfDay(15, minute)` - group rounds by the time of day they were posted at, in 15-minute intervals.

*Returns:*
A number of seconds representing the relative time of the beginning of the interval in which the group of rounds fall.
For example: `"winTimeOfDay": 3600`

### Duration

A field that represents a span of time, for example [solveTime](/datatypes/round.md#solveTime).
Rounds will be grouped based on their duration falling into specific intervals determined by the arguments provided.

*Arguments:*
* interval (integer): The number of the given unit to use as an interval.
* unit (string): The unit to use for the interval. Allowed values are `second`, `minute`, `hour`.

*Examples:*
* `?groupBy=solveTime(20, minute)` - group rounds by their time to solve, in 20-minute intervals.
* `?groupBy=postDelay(15, second)` - group rounds by their delay to post, in 15-second intervals.

*Returns:*
A number of seconds representing a lower limit (inclusive) of the interval in which the group of rounds fall.
For example, `"plusCorrectDelay": 15`
