# Player Counts

As an extension of the [Leaderboard](/endpoints/leaderboard.md) endpoint, you can use this endpoint to return just the number of players who have won rounds in the specified range.

**Note**: This endpoint is deprecated. All new integrations should utilize the [Round Aggregates](/endpoints/rounds-aggregate) endpoint, selecting the [numWinners](/datatypes/round-aggregates#numWinners) property.

## Details

* Method: `GET`
* Path: `/leaderboard/count`

## Query Parameters

### filterRounds
An expression by which to filter the rounds included in the leaderboard. See [Filters](/guides/filters.md) for more information.
* Type: `string`
* Example: `?filter=postTime gte 2019-01-01`

### *fromRound*
The round number for the earliest round to include in the leaderboard (inclusive)

**Deprecated**: Use `filterRounds` instead
* Type: `integer`
* Example: `?fromRound=50000`

### *toRound*
The round number for the latest round to include in the leaderboard (inclusive)

**Deprecated**: Use `filterRounds` instead
* Type: `integer`
* Example: `?toRound=50000`

### *fromTime*
Only include rounds that were won at or after this time. Specified as an ISO-8601 DateTime string, defaulting to UTC.

**Deprecated**: Use `filterRounds` instead
* Type: `string`
* Example: `?fromTime=2019-01-01`

### *toTime*
Only include rounds that were won at or before this time. Specified as an ISO-8601 DateTime string, defaulting to UTC.

**Deprecated**: Use `filterRounds` instead
* Type: `string`
* Example: `?toTime=2019-01-31T23:59:59`

## Returns

A JSON object with the following properties:

### numWinners
The numbers of players who have won rounds matching your search criteria.
* Type: `integer`

## Example Requests

* Get the number of players who won rounds in 2018:
    * `/leaderboard/count?filterRounds=winTime gte 2018-01-01 and winTime lt 2019-01-01`
* Get the number of players who won rounds between 40000 and 49999 (inclusive):
    * `/leaderboard/count?filterRounds=roundNumber gte 40000 and roundNumber lt 50000`
