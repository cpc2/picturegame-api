# Bulk Requests

The **Bulk Request** endpoint can be used to perform multiple data requests in a single HTTP request. This can be useful minimise network overheads and to avoid rate limits.

**Note**: This endpoint is considered an advanced feature. As an HTTP `POST` request is required, you will need to use an application like [Postman](https://www.postman.com/) to make use of it; you will not be able to simply use a web browser as you can with most other endpoints.

## Details

* Method: `POST`
* Path: `/bulk`

## Request Body

The request should be accompanied with a JSON object defining the requests, containing the following field:

### requests

A JSON array containing objects defining the individual requests to be carried out. See below for the format of each request object.

## Single Request

A JSON object defining the endpoint and associated parameters for the individual request to be performed.
This object must provide a single string field `"endpoint"` along with any other parameters that would normally be passed as query parameters for the given endpoint. The following endpoints are currently supported:

| `"endpoint"` Value | Details |
| :-- | :-- |
| `"leaderboard"` | [Leaderboard](/endpoints/leaderboard.md) |
| `"rounds"` | [List Rounds](/endpoints/rounds-list.md) |
| `"rounds/aggregate"` | [Get Round Aggregates](/endpoints/rounds-aggregate.md) |

### Parameters

Where in the individual endpoints you would provide query parameters, in a bulk request this information is provided as fields of the request object, alongside the endpoint name. All query parameters that can be specified on the individual endpoints can be specified in this way, with the exception of the deprecated parameters `fromRound`, `toRound`, `fromTime`, and `toTime` on the `leaderboard` endpoint.

## Response Body

The response will be a JSON object with a single field:

### responses

A JSON array of objects containing the responses for each individual request, in the same order they were provided in the request.
The contents of each individual response object will be identical to the output if the request were performed standalone; see the page for the specific endpoint for more information.

## Example Requests

* Request a leaderboard, list of round titles, and average solve time, for rounds hosted by Provium:

```json
{
    "requests": [
        {
            "endpoint": "leaderboard",
            "includeRoundNumbers": false,
            "filterRounds": "hostName eq 'provium'"
        },
        {
            "endpoint": "rounds",
            "filter": "hostName eq 'provium'",
            "select": ["title"]
        },
        {
            "endpoint": "rounds/aggregate",
            "filter": "hostName eq 'provium'",
            "select": ["avgSolveTime"]
        }
    ]
}
```

* Request the average solve time for rounds hosted by TheLamestUsername, and rounds won by TheLamestUsername:

```json
{
    "requests": [
        {
            "endpoint": "rounds/aggregate",
            "filter": "hostName eq 'TheLamestUsername'",
            "select": ["avgSolveTime"]
        },
        {
            "endpoint": "rounds/aggregate",
            "filter": "winnerName eq 'TheLamestUsername'",
            "select": ["avgSolveTime"]
        }
    ]
}
```

* Request the number of rounds per week and per month in 2021:

```json
{
    "requests": [
        {
            "endpoint": "rounds/aggregate",
            "filter": "postTime gte 2021-01-01 and postTime lt 2022-01-01",
            "groupBy": "postTime(1, week)"
        },
        {
            "endpoint": "rounds/aggregate",
            "filter": "postTime gte 2021-01-01 and postTime lt 2022-01-01",
            "groupBy": "postTime(1, month)"
        }
    ]
}
```
