# View Rounds

The **View Rounds** endpoint can be used to navigate to a specific round on Reddit.

## Details

* Method: `GET`
* Path: `/view/round/{roundNumber}` where `roundNumber` is an integer

## Returns

An HTTP 302 redirect to the thread for the specified round on Reddit.

## Example Requests

* Navigate to the Reddit thread for Round 40000:
    * `/view/round/40000`
