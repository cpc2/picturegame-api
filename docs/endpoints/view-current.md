# View Current Round

The **View Current** endpoint can be used to navigate to the current round on Reddit.

## Details

* Method: `GET`
* Path: `/view/current`

## Returns

An HTTP 302 redirect to the thread for the current round on Reddit.
If there is no ongoing round, the most recently solved round will be used.
