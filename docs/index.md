# PictureGame API

The PictureGame API is an HTTP API used for generating statistical data about rounds and players of [/r/PictureGame](https://reddit.com/r/picturegame).
The API is used as a back-end for both the PictureGame [Reddit Bot](https://gitlab.com/picturegame/picturegame-bot) and [Discord Bot](https://gitlab.com/picturegame/picturegame-discord-bot), with the former being its primary source of data.

The API can be used to [generate leaderboards](/endpoints/leaderboard) for specific date ranges or round number ranges, and view [filtered lists of rounds](/endpoints/rounds-list). More functionality such as high-level aggregates and calculated fields will be added in the future.

### Note

The current PictureGame Reddit Bot was launched in February 2017, at **Round 31867**. Prior to this round, there was no bot ensuring players had the correct round number in their title. As a result, the round number in the title of most rounds before 31867 does not match the actual round number of the round.

### Feedback

If you have found a bug in the API, or have a suggestion for new functionality, please let me know! Either [submit an issue](https://gitlab.com/picturegame/picturegame-api/issues/new), or contact Provium on Reddit or in our Discord server.
