import * as internal from './internal';

export function convertSortQuery(input: internal.SortItem<any>[]): string[] {
    return input.map(s => s.direction ? `${s.field} ${s.direction}` : s.field);
}
