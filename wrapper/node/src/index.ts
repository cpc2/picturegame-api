import fetch from 'node-fetch';
import * as QueryString from 'query-string';

import * as internal from './internal';
import * as model from './model';

import { convertSortQuery } from './QueryBuilder';

export * from './internal';
export * from './model';
export * from './querylang';

export interface PictureGameArgs {
    apiUrl: string;
    username?: string;
    password?: string;
}

export class PictureGame {
    constructor(private args: PictureGameArgs) {
    }

    async getCurrent(): Promise<model.Round> {
        const response = await this.sendRequest('/current') as model.CurrentResponse;
        return response.round;
    }

    async getLeaderboard(query: internal.LeaderboardQuery = {}): Promise<model.LeaderboardEntry[]> {
        const url = `/leaderboard?${QueryString.stringify(query)}`;
        const response = await this.sendRequest(url) as model.LeaderboardResponse;
        return response.leaderboard;
    }

    async getLeaderboardCount(query: internal.LeaderboardCountQuery = {}): Promise<number> {
        const url = `/leaderboard/count?${QueryString.stringify(query)}`;
        const response = await this.sendRequest(url) as model.LeaderboardCountResponse;
        return response.numWinners;
    }

    async migratePlayer(fromName: string, toName: string): Promise<model.LeaderboardEntry> {
        const response = await this.sendRequest(
            `/players/migrate?fromName=${fromName}&toName=${toName}`, 'POST') as model.MigrateResponse;
        return response.newPlayer;
    }

    async getRound(roundNumber: number): Promise<model.Round> {
        return await this.sendRequest(`/rounds/${roundNumber}`) as model.Round;
    }

    async getRounds(query: internal.RoundsQuery): Promise<model.ListResponse<model.Round>> {
        const queryString = QueryString.stringify({
            ...query,
            sort: query.sort && convertSortQuery(query.sort),
        });
        return await this.sendRequest(`/rounds?${queryString}`) as model.ListResponse<model.Round>;
    }

    async getRoundAggregates(query: internal.RoundAggregatesQuery): Promise<model.ListResponse<model.RoundGroup>> {
        const queryString = QueryString.stringify({
            ...query,
            groupSort: query.groupSort && convertSortQuery(query.groupSort),
        });
        return await this.sendRequest(`/rounds/aggregate?${queryString}`) as model.ListResponse<model.RoundGroup>;
    }

    async createRound(round: model.Round): Promise<model.UpdateRoundResponse> {
        return await this.sendRequest('/rounds', 'POST', round) as model.UpdateRoundResponse;
    }

    async editRound(round: model.Round): Promise<model.UpdateRoundResponse> {
        return await this.sendRequest(`/rounds/${round.roundNumber}`, 'PATCH', round) as model.UpdateRoundResponse;
    }

    async deleteRound(roundNumber: number): Promise<model.UpdateRoundResponse> {
        return await this.sendRequest(`/rounds/${roundNumber}`, 'DELETE') as model.UpdateRoundResponse;
    }

    async getStatus(): Promise<model.StatusResponse> {
        return await this.sendRequest('/status') as model.StatusResponse;
    }

    async setStatus(status: model.ServiceStatusRequest): Promise<model.ServiceStatusResponse> {
        return await this.sendRequest('/status', 'POST', status) as model.ServiceStatusResponse;
    }

    private async sendRequest(endpoint: string, method?: string, data?: any) {
        const headers: { [header: string]: string } = {};
        if (typeof data === 'object') {
            headers['Content-Type'] = 'application/json';
        }
        if (this.args.username && this.args.password) {
            headers['Authorization'] = 'Basic ' + btoa(`${this.args.username}:${this.args.password}`);
        }

        const res = await fetch(this.args.apiUrl + endpoint, {
            body: typeof data === 'object' ? JSON.stringify(data) : data,
            method,
            headers,
        });

        if (res.status < 200 || res.status >= 300) {
            throw new Error(`Unexpected status code ${res.status}`);
        }

        return await res.json();
    }
}
