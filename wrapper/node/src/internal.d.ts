import * as Model from './model';

export interface RoundsQuery {
    filter?: string;
    select?: (keyof Model.Round)[];
    sort?: SortItem<Model.Round>[];

    offset?: number;
}

export interface RoundAggregatesQuery {
    filter?: string;
    select?: (keyof Model.RoundAggregates)[];

    groupBy?: keyof Model.Round;
    groupSort?: SortItem<Model.RoundGroup>[];
    groupFilter?: string;

    offset?: number;
}

export interface LeaderboardQuery {
    fromRank?: number;
    count?: number;
    players?: string[];
    ranks?: number[];

    includeRoundNumbers?: boolean;

    filterRounds?: string;
}

export interface LeaderboardCountQuery {
    filterRounds?: string;
}

export interface SortItem<T> {
    field: (keyof T) & string;
    direction?: 'asc' | 'desc';
}
