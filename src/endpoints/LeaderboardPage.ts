import * as express from 'express';

import { Config, Response } from '../index.d';

import { LeaderboardController } from '../controllers/LeaderboardController';

import { DbWrapper } from '../db';

import * as Query from '../models/Query';

import Endpoint from './Endpoint';

export function configureLeaderboard(config: Config, db: DbWrapper): express.Router {
    const endpoint = new LeaderboardPage(config, db);
    const router = express.Router();

    router.get('/count', endpoint.getPlayerCount);
    router.get('/', endpoint.getLeaderboard);

    return router;
}

class LeaderboardPage extends Endpoint {
    getLeaderboard = this.configureEndpoint(async (req: express.Request, res: Response) => {
        const controller = new LeaderboardController(this.db, res.locals.logger);

        const leaderboard = await controller.getLeaderboardSubset({
            // Filter rounds (deprecated)
            fromRound: Query.positiveInt(req.query, 'fromRound'),
            toRound: Query.positiveInt(req.query, 'toRound'),
            fromTime: Query.dateTime(req.query, 'fromTime'),
            toTime: Query.dateTime(req.query, 'toTime'),

            // Filter resulting players/ranks
            fromRank: Query.positiveInt(req.query, 'fromRank') || 1,
            count: Query.positiveInt(req.query, 'count'),
            players: Query.array(req.query, 'players'),
            ranks: Query.array(req.query, 'ranks'),

            includeRoundNumbers: Query.bool(req.query, 'includeRoundNumbers', true),
            includeWinTimes: Query.bool(req.query, 'includeWinTimes', false),

            // Query language-based filter
            // not just "filter" as we may want to have filterPlayers later, too
            filterRounds: Query.extractStringOrThrow(req.query, 'filterRounds'),
        });

        res.json(leaderboard);
    });

    getPlayerCount = this.configureEndpoint(async (req, res) => {
        const controller = new LeaderboardController(this.db, res.locals.logger);

        const response = await controller.getPlayerCount({
            fromRound: Query.positiveInt(req.query, 'fromRound'),
            toRound: Query.positiveInt(req.query, 'toRound'),
            fromTime: Query.dateTime(req.query, 'fromTime'),
            toTime: Query.dateTime(req.query, 'toTime'),
            filterRounds: Query.extractStringOrThrow(req.query, 'filterRounds'),
        });

        res.json(response);
    });
}
