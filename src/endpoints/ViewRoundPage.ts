import * as express from 'express';

import { Config } from '../index.d';

import { RoundsController } from '../controllers';
import { DbWrapper } from '../db';
import { NotFoundError } from '../models/Error';

import Endpoint from './Endpoint';

export function configureViewRounds(config: Config, db: DbWrapper): express.Router {
    const endpoint = new ViewRoundPage(config, db);
    const router = express.Router();

    router.get('/current', endpoint.viewCurrentRound);
    router.get('/round/:roundNumber', endpoint.viewRound);

    return router;
}

class ViewRoundPage extends Endpoint {
    viewCurrentRound = this.configureEndpoint(async (_, res) => {
        const currentRound = await this.db.rounds.getLatestRound();
        res.redirect(`https://redd.it/${currentRound.id}`);
    });

    viewRound = this.configureEndpoint(async (req, res) => {
        const roundsController = new RoundsController(this.db, res.locals.logger);
        const round = await roundsController.getRoundForNumber(req.params.roundNumber);
        if (!round.id) {
            throw new NotFoundError(NotFoundError.UnknownRoundId);
        }
        res.redirect(`https://redd.it/${round.id}`);
    });
}
