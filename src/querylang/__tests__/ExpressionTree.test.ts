import { ExpressionTreeBuilder, Tokenizer } from 'picturegame-api-wrapper';

const SuccessfulInputs = [
    'a eq 2000',
    '(((((a eq 2000)))))', // Lots of useless brackets, shouldn't make a difference to output
    'a eq 1 and b eq 2',
    'a eq 1 or b eq 2',
    'a eq 1 and b eq 2 and c eq 3',
    'a eq 1 or b eq 2 or c eq 3',
    'a eq 1 and b eq 2 or c eq 3 and d eq 4',
    'a eq 1 and (b eq 2 or c eq 3) and d eq 4',
    'a eq [1, 2, 3]',
    'a eq ["a",\'b\',"c"]',
    'a eq [1,]',
];

const ErrorInputs = [
    'eq',
    'a',
    'a a',
    'a eq',
    'a eq eq',
    '',
    '()',
    'and',
    'a and',
    'a eq and',
    '(a eq 1',
    'a (',
    'a eq (',
    'a eq 1)',
    'a eq 1 (',
    'asc',
    'a asc',
    'a eq asc',
    'a eq 1 asc',
    'a eq [',
    'a eq [,',
    'a eq [1',
    'a eq [1,',
    'a eq [1(',
    'a eq [1,"a"]',
    'a eq [1,)]',
];

describe('ExpressionTreeBuilder', () => {
    describe('Success state snapshots', () => {
        for (const input of SuccessfulInputs) {
            test(input, () => {
                const tokens = new Tokenizer(input).tokenize();
                expect(new ExpressionTreeBuilder(tokens).read().raw()).toMatchSnapshot();
            });
        }
    });

    describe('Error state snapshots', () => {
        for (const input of ErrorInputs) {
            test(input, () => {
                function test() {
                    const tokens = new Tokenizer(input).tokenize();
                    new ExpressionTreeBuilder(tokens).read();
                }
                expect(test).toThrowErrorMatchingSnapshot();
            });
        }
    });
});
