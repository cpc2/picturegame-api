import { getTokenTypeOrThrow, FieldSpec, QueryParseError, Tokenizer, TokenType } from 'picturegame-api-wrapper';
import { QueryContext } from './QueryBuilder';

export function getSelectedFields<T>(input: string[], context: QueryContext<T>, spec: FieldSpec<T>) {
    for (const rawField of input) {
        const tokens = new Tokenizer(rawField).tokenize();
        if (tokens.length !== 1 || tokens[0].type !== TokenType.Identifier) {
            throw new QueryParseError('Elements in parameter ?select must be single identifiers.');
        }
        getTokenTypeOrThrow(tokens[0], spec);
        context.selectColumn(tokens[0].input as keyof T);
    }
}
