import { ApiError } from '../../models/Error';

export * from './TestServer';

export async function expectToThrowError(fn: () => Promise<any>, error: ApiError) {
    try {
        await fn();
        expect('No error thrown').toBe('Error is thrown');
    } catch (e) {
        expect((e as ApiError)).toEqual(error);
    }
}

export async function expectNoError(fn: () => Promise<any>) {
    try {
        await fn();
    } catch (e) {
        expect(e).toBeUndefined();
        expect('Error thrown').toBe('No error thrown');
    }
}
