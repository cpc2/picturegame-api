import * as request from 'supertest';

import { Application } from '../application';
import { BadRequestError, NotFoundError, ValidationErrorCode } from '../models/Error';
import { Round } from '../models/model';

import * as utils from './testutils';

describe('/rounds integration tests', () => {
    let app: Application;

    async function setupDummyRounds() {
        await app.db.execSql([
            'INSERT OR REPLACE INTO rounds(round_number, winner_name) VALUES (1, "Provium");',
            'DELETE FROM rounds WHERE round_number = 2;',
        ]);
    }

    beforeAll(async () => {
        app = await utils.getTestServer({ api: { authBypass: true } });
    });

    afterAll(async () => {
        await app?.teardown();
    });

    describe('Round trip round fields', () => {
        beforeAll(setupDummyRounds);

        it('should be able to create, update and retrieve all fields', async () => {
            const initialRound: Round = {
                roundNumber: 2,
                hostName: 'host',
                id: 'id',
                plusCorrectTime: 3,
                postTime: 1,
                postUrl: 'url',
                thumbnailUrl: 'thumb',
                title: 'title',
                winnerName: 'winner',
                winningCommentId: 'comment',
                winTime: 2,
                abandonedTime: 4,
            };

            await request(app.app).post('/rounds').send(initialRound).expect(201);

            let res = await request(app.app).get('/rounds/2').expect(200);
            expect(res.body).toMatchObject(initialRound);

            const updated: Round = {
                roundNumber: 2,
                hostName: 'host2',
                id: 'id2',
                plusCorrectTime: 4,
                postTime: 2,
                postUrl: 'url2',
                thumbnailUrl: 'thumb2',
                title: 'title2',
                winnerName: 'winner2',
                winningCommentId: 'comment2',
                winTime: 3,
                abandonedTime: 5,
            };

            await request(app.app).patch('/rounds/2').send(updated).expect(200);

            res = await request(app.app).get('/rounds/2').expect(200);
            expect(res.body).toMatchObject(updated);
        });
    });

    describe('#GET /rounds/:roundNumber', () => {

        beforeAll(setupDummyRounds);

        it('should return 400 if roundNumber is not a number', async () => {
            const res = await request(app.app).get('/rounds/a').expect(400);

            expect(res.body.data.errors
                .filter((e: string) => e === ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'))
                .length).toBe(1);
        });

        it('should return 400 if roundNumber is negative', async () => {
            const res = await request(app.app).get('/rounds/-1').expect(400);

            expect(res.body.data.errors
                .filter((e: string) => e === ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'))
                .length).toBe(1);
        });

        it('should return 400 if roundNumber is a float', async () => {
            const res = await request(app.app).get('/rounds/1.5').expect(400);

            expect(res.body.data.errors
                .filter((e: string) => e === ValidationErrorCode.ValueMustBePositiveInteger('roundNumber'))
                .length).toBe(1);
        });

        it('should return 200 if the round is found', async () => {
            const res = await request(app.app).get('/rounds/1').expect(200);
            expect(res.body.roundNumber).toBe(1);
        });

        it('should return 404 if the round is not found', async () => {
            const res = await request(app.app).get('/rounds/2').expect(404);
            expect(res.body).toEqual(new NotFoundError(NotFoundError.RoundNotFound));
        });
    });

    describe('#POST /rounds', () => {

        beforeEach(setupDummyRounds);

        it('should return a 400 if the body is empty', async () => {
            const res = await request(app.app).post('/rounds').send({}).expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.EmptyBodyError));
        });

        it('should return a 400 for validation errors', async () => {
            const res = await request(app.app).post('/rounds').send({ roundNumber: 'a' }).expect(400);
            expect(Array.isArray(res.body.data.errors)).toBe(true);
            expect(res.body.data.errors).not.toHaveLength(0);
        });

        it('should return a 400 if the round already exists', async () => {
            const res = await request(app.app).post('/rounds')
                .send({ roundNumber: 1 })
                .expect(400);

            expect(res.body).toEqual(new BadRequestError(BadRequestError.RoundAlreadyExists));
        });

        it('should create rounds in the database', async () => {
            const round: Round = { roundNumber: 2 };

            await request(app.app).post('/rounds').send(round);
            const res = await request(app.app).get(`/rounds/${round.roundNumber}`).expect(200);

            expect(res.body).toMatchObject(round);
        });

        it('should return the inserted round and the winner\'s win list', async () => {
            const round: Round = {
                roundNumber: 2,
                winTime: 123,
            };
            const res = await request(app.app).post('/rounds').send(round).expect(201);

            expect(res.body.round).toEqual(round);
            expect(res.body.newWinner).toBeUndefined();
        });

        it('should return the winner\'s win list', async () => {
            const round: Round = {
                roundNumber: 2,
                winnerName: 'Provium',
            };
            const res = await request(app.app).post('/rounds').send(round).expect(201);

            expect(res.body.newWinner).toBeDefined();
            expect(res.body.newWinner.username).toBe(round.winnerName);
            expect(res.body.newWinner.roundList).toEqual(expect.arrayContaining([round.roundNumber]));
        });
    });

    describe('#PATCH /rounds', () => {

        beforeEach(setupDummyRounds);

        it('should return a 400 if the body is empty', async () => {
            const res = await request(app.app).patch('/rounds/1').send({}).expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.EmptyBodyError));
        });

        it('should return 400 if the round number is invalid', async () => {
            const res = await request(app.app).patch('/rounds/a').send({ a: 1 }).expect(400);

            expect(res.body.data.errors).toEqual(
                expect.arrayContaining([ValidationErrorCode.ValueMustBePositiveInteger('roundNumber')]));
        });

        it('should update rounds in the database', async () => {
            await request(app.app).patch('/rounds/1').send({ hostName: 'Provium2' });
            const res = await request(app.app).get('/rounds/1').expect(200);

            expect(res.body.hostName).toBe('Provium2');
        });

        it('should validate the merged round', async () => {
            const res = await request(app.app).patch('/rounds/1').send({ hostName: 'Provium' }).expect(400);
            expect(res.body.data.errors).toEqual(expect.arrayContaining([ValidationErrorCode.RoundCannotBeWonByHost]));
        });

        it('should return the updated round', async () => {
            const res = await request(app.app).patch('/rounds/1').send({ winnerName: 'Provium2' }).expect(200);

            expect(res.body.round).toBeDefined();
            expect(res.body.round.roundNumber).toBe(1);
            expect(res.body.round.winnerName).toBe('Provium2');
        });

        it('should return the old winner\'s stats', async () => {
            const res = await request(app.app).patch('/rounds/1').send({ winnerName: 'Provium2' }).expect(200);

            expect(res.body.oldWinner).toBeDefined();
            expect(res.body.oldWinner.username).toBe('Provium');
            expect(Array.isArray(res.body.oldWinner.roundList)).toBe(true);
            expect(res.body.oldWinner.roundList).not.toEqual(expect.arrayContaining([1]));
        });

        it('should return the new winner\'s stats', async () => {
            const res = await request(app.app).patch('/rounds/1').send({ winnerName: 'Provium2' }).expect(200);

            expect(res.body.newWinner).toBeDefined();
            expect(res.body.newWinner.username).toBe('Provium2');
            expect(Array.isArray(res.body.newWinner.roundList)).toBe(true);
            expect(res.body.newWinner.roundList).toEqual(expect.arrayContaining([1]));
        });
    });

    describe('#DELETE /rounds', () => {

        beforeEach(setupDummyRounds);

        it('should return 400 if the round number is invalid', async () => {
            const res = await request(app.app).delete('/rounds/a').expect(400);

            expect(Array.isArray(res.body.data.errors)).toBe(true);
            expect(res.body.data.errors).toEqual(
                expect.arrayContaining([ValidationErrorCode.ValueMustBePositiveInteger('roundNumber')]));
        });

        it('should return 404 if the round doesn\'t exist', async () => {
            const res = await request(app.app).delete('/rounds/2').expect(404);
            expect(res.body).toEqual(new NotFoundError(NotFoundError.RoundNotFound));
        });

        it('should return the deleted round and the old winner\'s stats', async () => {
            const res = await request(app.app).delete('/rounds/1').expect(200);

            expect(res.body.round).toBeDefined();
            expect(res.body.oldWinner).toBeDefined();
            expect(res.body.round.roundNumber).toBe(1);
            expect(res.body.oldWinner.username).toBe('Provium');
            expect(Array.isArray(res.body.oldWinner.roundList)).toBe(true);
            expect(res.body.oldWinner.roundList).not.toEqual(expect.arrayContaining([2]));
        });
    });
});
