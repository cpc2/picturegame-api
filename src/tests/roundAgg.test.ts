import * as request from 'supertest';

import { Application } from '../application';
import { LeaderboardResponse, ListResponse, RoundGroup } from '../models/model';

import * as utils from './testutils';

type AggResponse = ListResponse<RoundGroup>;

const allFields: (keyof RoundGroup)[] = [
    'avgPlusCorrectDelay', 'maxPlusCorrectDelay', 'minPlusCorrectDelay',
    'avgPostDelay', 'maxPostDelay', 'minPostDelay',
    'avgSolveTime', 'maxSolveTime', 'minSolveTime',
    'maxPostTime', 'minPostTime',
    'maxWinTime', 'minWinTime',
    'maxRoundNumber', 'minRoundNumber',
    'numRounds', 'numHosts', 'numWinners',
];

const TenRoundFilter = 'filter=roundNumber gte 50050 and roundNumber lt 50060';

describe('GET /rounds/aggregate', () => {
    let app: Application;

    function configure(opts: utils.ConfigureTestOpts = {}) {
        beforeAll(async () => {
            app = await utils.createAppWithData(opts);
        });

        afterAll(async () => {
            await app?.teardown();
        });
    }

    describe('with no grouping', () => {
        configure();

        it('should compute global aggregates', async () => {
            const res = await request(app.app).get(`/rounds/aggregate?select=${allFields.join(',')}`).expect(200);
            const body = res.body as AggResponse;

            expect(body.totalNumResults).toBe(1);
            expect(body.results.length).toBe(1);
            expect(body.results[0]).toMatchSnapshot();
        });

        it('should filter the rounds it considers', async () => {
            const select = 'select=numRounds,minRoundNumber,maxRoundNumber';
            const res = await request(app.app).get(`/rounds/aggregate?${TenRoundFilter}&${select}`).expect(200);
            const body = res.body as AggResponse;

            expect(body.totalNumResults).toBe(1);
            expect(body.results.length).toBe(1);
            expect(body.results[0]).toEqual({
                numRounds: 10,
                minRoundNumber: 50050,
                maxRoundNumber: 50059,
            });
        });

        it('should always return numRounds', async () => {
            const res = await request(app.app).get('/rounds/aggregate').expect(200);
            const body = res.body as AggResponse;
            expect(body.results[0]).toEqual({ numRounds: 200 });
        });
    });

    describe('with grouping', () => {
        configure();

        it('should group results', async () => {
            const res = await request(app.app)
                .get(`/rounds/aggregate?groupBy=winnerName&${TenRoundFilter}`)
                .expect(200);
            const body = res.body as AggResponse;
            expect(body.totalNumResults).toBe(9);
            const results = [...body.results].sort((a, b) => a.winnerName! > b.winnerName! ? 1 : -1);
            expect(results).toMatchSnapshot();
        });

        it('should return 400 for grouping by unsupported fields', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=roundNumber').expect(400);
            expect(res.body).toMatchSnapshot();
        });

        it('should filter groups', async () => {
            const res = await request(app.app)
                .get(`/rounds/aggregate?groupBy=winnerName&${TenRoundFilter}&groupFilter=numRounds gt 1`)
                .expect(200);
            const body = res.body as AggResponse;
            expect(body.totalNumResults).toBe(1);
            expect(body.results.length).toBe(1);
            expect(body.results[0]).toEqual({
                winnerName: 'SuperFreakonomics',
                numRounds: 2,
            });
        });

        it('should sort groups', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&groupSort=numRounds desc')
                .expect(200);
            const leaderboard = (await request(app.app)
                .get('/leaderboard?fromRound=50000&toRound=501999&includeRoundNumbers=false'))
                .body as LeaderboardResponse;

            const body = res.body as AggResponse;
            expect(body.totalNumResults).toBe(leaderboard.leaderboard.length);
            expect(body.results.map(r => r.numRounds))
                .toEqual(leaderboard.leaderboard.slice(0, 10).map(p => p.numWins));
        });

        it('should support multiple levels of group sorting', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&groupSort=numRounds desc,maxRoundNumber asc')
                .expect(200);
            const leaderboard = (await request(app.app)
                .get('/leaderboard?fromRound=50000&toRound=501999&includeRoundNumbers=false'))
                .body as LeaderboardResponse;

            const body = res.body as AggResponse;
            expect(body.totalNumResults).toBe(leaderboard.leaderboard.length);

            const expected = leaderboard.leaderboard.slice(0, 10);
            expect(body.results.map(r => r.numRounds)).toEqual(expected.map(p => p.numWins));
            expect(body.results.map(r => r.winnerName)).toEqual(expected.map(p => p.username));
        });

        it('should offset the list of results', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&groupSort=numRounds desc&offset=10')
                .expect(200);
            const leaderboard = (await request(app.app)
                .get('/leaderboard?fromRound=50000&toRound=501999&includeRoundNumbers=false'))
                .body as LeaderboardResponse;

            const body = res.body as AggResponse;
            expect(body.totalNumResults).toBe(leaderboard.leaderboard.length);

            const expected = leaderboard.leaderboard.slice(10, 20);
            expect(body.results.map(r => r.numRounds)).toEqual(expected.map(p => p.numWins));
        });

        it('should return sorted list of win times', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&select=allWinTimes')
                .expect(200);
            const body = res.body as AggResponse;
            for (const agg of body.results) {
                expect(agg.allWinTimes).not.toBeNull();
                expect(agg.allWinTimes!.length).toBe(agg.numRounds);
                for (let i = 1; i < agg.allWinTimes!.length; ++i) {
                    expect(agg.allWinTimes![i - 1]).toBeLessThanOrEqual(agg.allWinTimes![i]);
                }
            }
        });
    });

    describe('sorting groups with null aggs', () => {
        configure({ dataFile: 'MockData_NullValues.csv' });

        it('should sort nulls at the end for asc', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&groupSort=minSolveTime asc')
                .expect(200);
            const body = res.body as AggResponse;

            const usernames = body.results.map(g => g.winnerName);
            expect(usernames).toEqual(['c', 'a', 'b']);
        });

        it('should sort nulls at the end for desc', async () => {
            const res = await request(app.app)
                .get('/rounds/aggregate?groupBy=winnerName&groupSort=minSolveTime desc')
                .expect(200);
            const body = res.body as AggResponse;

            const usernames = body.results.map(g => g.winnerName);
            expect(usernames).toEqual(['a', 'c', 'b']);
        });
    });

    describe('grouping by absolute times', () => {
        configure({ dataFile: 'MockData_PostTimes.csv' });

        test('one-hour interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(1, hour)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1640991600, numRounds: 1 },
                { postTime: 1640995200, numRounds: 2 },
                { postTime: 1641092400, numRounds: 1 },
                { postTime: 1641225600, numRounds: 1 },
                { postTime: 1643752800, numRounds: 1 },
                { postTime: 1643756400, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('three-hour interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(3, hour)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1640984400, numRounds: 1 },
                { postTime: 1640995200, numRounds: 2 },
                { postTime: 1641092400, numRounds: 1 },
                { postTime: 1641222000, numRounds: 1 },
                { postTime: 1643749200, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('one-day interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(1, day)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1640908800, numRounds: 1 },
                { postTime: 1640995200, numRounds: 2 },
                { postTime: 1641081600, numRounds: 1 },
                { postTime: 1641168000, numRounds: 1 },
                { postTime: 1643673600, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('five-day interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(5, day)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1640736000, numRounds: 4 },
                { postTime: 1641168000, numRounds: 1 },
                { postTime: 1643328000, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('one-week interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(1, week)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1640476800, numRounds: 3 },
                { postTime: 1641081600, numRounds: 2 },
                { postTime: 1643500800, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('one-month interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(1, month)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1638316800, numRounds: 1 },
                { postTime: 1640995200, numRounds: 4 },
                { postTime: 1643673600, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('two-month interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(2, month)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1635724800, numRounds: 1 },
                { postTime: 1640995200, numRounds: 6 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('one-year interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(1, year)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1609459200, numRounds: 1 },
                { postTime: 1640995200, numRounds: 6 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('ten-year interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTime(10, year)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTime: 1577836800, numRounds: 7 },
            ];
            expect(body.results).toEqual(expected);
        });
    });

    describe('grouping by relative times', () => {
        configure({ dataFile: 'MockData_PostTimes.csv' });

        test('one-hour interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTimeOfDay(1, hour)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTimeOfDay: 0, numRounds: 2 },
                { postTimeOfDay: 10800, numRounds: 1 },
                { postTimeOfDay: 57600, numRounds: 1 },
                { postTimeOfDay: 79200, numRounds: 1 },
                { postTimeOfDay: 82800, numRounds: 2 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('four-hour interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTimeOfDay(4, hour)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTimeOfDay: 0, numRounds: 3 },
                { postTimeOfDay: 57600, numRounds: 1 },
                { postTimeOfDay: 72000, numRounds: 3 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('fifteen-minute interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=postTimeOfDay(15, minute)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { postTimeOfDay: 0, numRounds: 1 },
                { postTimeOfDay: 900, numRounds: 1 },
                { postTimeOfDay: 11700, numRounds: 1 },
                { postTimeOfDay: 60300, numRounds: 1 },
                { postTimeOfDay: 79200, numRounds: 1 },
                { postTimeOfDay: 83700, numRounds: 1 },
                { postTimeOfDay: 85500, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });
    });

    describe('grouping by durations', () => {
        configure({ dataFile: 'MockData_PostTimes.csv' });

        test('five-minute interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=solveTime(5, minute)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { solveTime: 0, numRounds: 2 },
                { solveTime: 600, numRounds: 1 },
                { solveTime: 900, numRounds: 1 },
                { solveTime: 1800, numRounds: 1 },
                { solveTime: 3900, numRounds: 1 },
                { solveTime: 4800, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('fifteen-minute interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=solveTime(15, minute)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { solveTime: 0, numRounds: 3 },
                { solveTime: 900, numRounds: 1 },
                { solveTime: 1800, numRounds: 1 },
                { solveTime: 3600, numRounds: 1 },
                { solveTime: 4500, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('five-second interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=plusCorrectDelay(5, second)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { plusCorrectDelay: 10, numRounds: 2 },
                { plusCorrectDelay: 15, numRounds: 2 },
                { plusCorrectDelay: 20, numRounds: 1 },
                { plusCorrectDelay: 25, numRounds: 1 },
                { plusCorrectDelay: 30, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });

        test('thirty-second interval', async () => {
            const res = await request(app.app).get('/rounds/aggregate?groupBy=plusCorrectDelay(30, second)').expect(200);
            const body = res.body as AggResponse;

            const expected: RoundGroup[] = [
                { plusCorrectDelay: 0, numRounds: 6 },
                { plusCorrectDelay: 30, numRounds: 1 },
            ];
            expect(body.results).toEqual(expected);
        });
    });

    describe('validation of groupBy param', () => {
        configure();

        test('invalid time units for absolute', async () => {
            for (const unit of ['minute', 'second', 'blah', '4']) {
                await request(app.app).get(`/rounds/aggregate?groupBy=postTime(3, ${unit})`).expect(400);
            }
        });

        test('invalid time units for relative', async () => {
            for (const unit of ['year', 'month', 'week', 'day', 'second', 'blah', '4']) {
                await request(app.app).get(`/rounds/aggregate?groupBy=postTimeOfDay(3, ${unit})`).expect(400);
            }
        });

        test('invalid time units for duration', async () => {
            for (const unit of ['year', 'month', 'week', 'day', 'blah', '4']) {
                await request(app.app).get(`/rounds/aggregate?groupBy=solveTime(3, ${unit})`).expect(400);
            }
        });

        test('invalid input for interval', async () => {
            for (const interval of ['-5', 'a', '4.5', '$%^']) {
                await request(app.app).get(`/rounds/aggregate?groupBy=solveTime(${interval}, second)`).expect(400);
            }
        });

        test('wrong number of args for intervals', async () => {
            await request(app.app).get('/rounds/aggregate?groupBy=solveTime').expect(400);
            await request(app.app).get('/rounds/aggregate?groupBy=solveTime()').expect(400);
            await request(app.app).get('/rounds/aggregate?groupBy=solveTime(year)').expect(400);
            await request(app.app).get('/rounds/aggregate?groupBy=solveTime(1)').expect(400);
            await request(app.app).get('/rounds/aggregate?groupBy=solveTime(1, year, 1)').expect(400);
        });

        test('providing args for exact match', async () => {
            await request(app.app).get('/rounds/aggregate?groupBy=hostName()').expect(200); // Allowed empty parens
            await request(app.app).get('/rounds/aggregate?groupBy=hostName(1)').expect(400);
        });
    });
});
