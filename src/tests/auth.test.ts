import * as request from 'supertest';

import { Application } from '../application';
import { BadRequestError, UnauthorizedError } from '../models/Error';

import * as utils from './testutils';

const AuthNeededMethods: [string, keyof request.SuperTest<any>][] = [
    ['/rounds', 'post'],
    ['/rounds/1', 'patch'],
    ['/rounds/1', 'delete'],
    ['/players/migrate', 'post'],
];

describe('auth integration tests', () => {
    let app: Application;

    beforeAll(async () => {
        app = await utils.getTestServer({ api: { authBypass: false } });
        await app.db.users.addUser('Provium', 'password');
    });

    afterAll(async () => {
        await app?.teardown();
    });

    it('should give a 401 if no auth is provided', async () => {
        const res = await request(app.app).post('/rounds').expect(401);
        expect(res.body).toEqual(new UnauthorizedError(UnauthorizedError.AuthRequired));
    });

    it('should give a 401 if the user does not exist', async () => {
        const res = await request(app.app).post('/rounds').auth('NoName', 'password').expect(401);
        expect(res.body).toEqual(new UnauthorizedError(UnauthorizedError.InvalidLogin));
    });

    it('should give a 401 if the password is wrong', async () => {
        const res = await request(app.app).post('/rounds').auth('Provium', 'wrong').expect(401);
        expect(res.body).toEqual(new UnauthorizedError(UnauthorizedError.InvalidLogin));
    });

    it('should accept valid username and passwords', async () => {
        const res = await request(app.app).post('/rounds').auth('Provium', 'password').expect(400);
        expect(res.body).toEqual(new BadRequestError(BadRequestError.EmptyBodyError));
    });

    it('should be case-insensitive for usernames', async () => {
        const res = await request(app.app).post('/rounds').auth('provium', 'password').expect(400);
        expect(res.body).toEqual(new BadRequestError(BadRequestError.EmptyBodyError));
    });

    for (const details of AuthNeededMethods) {
        const [endpoint, method] = details;
        it(`should authenticate with ${method} ${endpoint}`, async () => {
            const res = await (request(app.app) as any)[method](endpoint).expect(401);
            expect(res.body).toEqual(new UnauthorizedError(UnauthorizedError.AuthRequired));
        });
    }
});
