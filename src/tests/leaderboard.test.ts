import * as _ from 'lodash';
import * as moment from 'moment';
import * as request from 'supertest';

import { Application } from '../application';
import { BadRequestError, NotFoundError } from '../models/Error';
import { LeaderboardEntry, LeaderboardResponse } from '../models/model';

import * as utils from './testutils';

describe('/leaderboard integration tests', () => {
    let app: Application;

    beforeAll(async () => {
        function insertRound(num: number, winner: string, start: string, end: string) {
            const startTime = moment.utc(start).unix();
            const endTime = moment.utc(end).unix();
            return 'INSERT INTO rounds(round_number, winner_name, post_time, win_time) ' +
                `VALUES(${num}, "${winner}", ${startTime}, ${endTime});`;
        }
        function insertUnsolvedRound(num: number) {
            return `INSERT INTO rounds(round_number) VALUES(${num});`;
        }
        /**
         * Sample leaderboard:
         *  Rank 1: Provium, rounds 1, 2, 3
         *  Rank 2: Provium2, rounds 4, 5, 6
         *  Rank 3: Provium3, rounds 7, 8
         */
        app = await utils.getTestServer({},
            insertRound(1, 'Provium', '2018-01-01T00:00:00', '2018-01-01T05:00:00'),
            insertRound(2, 'Provium', '2018-01-01T05:30:00', '2018-01-01T17:00:00'),
            insertRound(3, 'Provium', '2018-01-01T23:00:00', '2018-01-02T02:00:00'),
            insertRound(4, 'Provium2', '2018-01-02T02:30:00', '2018-01-02T03:00:00'),
            insertRound(5, 'Provium2', '2018-01-02T03:04:00', '2018-01-02T16:29:00'),
            insertRound(6, 'Provium2', '2018-01-02T17:00:00', '2018-01-02T22:00:00'),
            insertRound(7, 'Provium3', '2018-01-02T22:15:00', '2018-01-03T02:00:00'),
            insertRound(8, 'Provium3', '2018-01-03T03:00:00', '2018-01-03T12:00:00'),
            insertUnsolvedRound(9),
            insertUnsolvedRound(10),
            insertUnsolvedRound(11),
            insertUnsolvedRound(12),
            insertUnsolvedRound(13),
            insertUnsolvedRound(14),
            insertUnsolvedRound(15),
        );
    });

    afterAll(async () => {
        await app?.teardown();
    });

    it('should return 404 if fromRank is too high', async () => {
        const res = await request(app.app).get('/leaderboard?fromRank=4').expect(404);
        expect(res.body).toEqual(new NotFoundError(NotFoundError.InvalidRank));
    });

    for (const fromRank of ['asd', '1a', '0', '-1']) {
        it(`should return 400 if fromRank is ${fromRank}`, async () => {
            const res = await request(app.app).get(`/leaderboard?fromRank=${fromRank}`).expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.InvalidPositiveInt, {
                fieldName: 'fromRank',
            }));
        });
    }

    for (const count of ['asd', '1a', '0', '-1']) {
        it(`should return 400 if count is ${count}`, async () => {
            const res = await request(app.app).get(`/leaderboard?count=${count}`).expect(400);
            expect(res.body).toEqual(new BadRequestError(BadRequestError.InvalidPositiveInt, {
                fieldName: 'count',
            }));
        });
    }

    it('should return 400 if an array is used for a string param', async () => {
        const res = await request(app.app).get(`/leaderboard?fromTime[]=2018-01-01`).expect(400);
        expect(res.body).toEqual(new BadRequestError(BadRequestError.ValueMustNotBeObject, {
            fieldName: 'fromTime',
        }));
    });

    it('should return 400 if an object is used for a string param', async () => {
        const res = await request(app.app).get('/leaderboard?fromTime[a]=2018-01-01').expect(400);
        expect(res.body).toEqual(new BadRequestError(BadRequestError.ValueMustNotBeObject, {
            fieldName: 'fromTime',
        }));
    });

    it('should return 400 if an object is used for an array param', async () => {
        const res = await request(app.app).get('/leaderboard?ranks[a]=1').expect(400);
        expect(res.body).toEqual(new BadRequestError(BadRequestError.ValueMustNotBeObject, {
            fieldName: 'ranks',
        }));
    });

    it('should return all ranks if there are no params', async () => {
        const res = await request(app.app).get('/leaderboard').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.map((p: LeaderboardEntry) => p.rank)).toEqual([1, 2, 3]);
        expect(body.leaderboard.map(p => p.numWins)).toEqual([3, 3, 2]);
    });

    it('should start at fromRank', async () => {
        const res = await request(app.app).get('/leaderboard?fromRank=2').expect(200);
        expect(res.body.leaderboard.map((p: LeaderboardEntry) => p.rank)).toEqual([2, 3]);
    });

    it('should include `count` players', async () => {
        const res = await request(app.app).get('/leaderboard?count=2').expect(200);
        expect(res.body.leaderboard.map((p: LeaderboardEntry) => p.rank)).toEqual([1, 2]);
    });

    it('should include win list by default', async () => {
        const res = await request(app.app).get('/leaderboard').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.every(p => _.isArray(p.roundList))).toBe(true);
    });

    it('should include win list if includeRoundNumbers=true', async () => {
        const res = await request(app.app).get('/leaderboard?includeRoundNumbers=true').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.every(p => _.isArray(p.roundList))).toBe(true);
    });

    it('should not include win list if includeRoundNumbers=false', async () => {
        const res = await request(app.app).get('/leaderboard?includeRoundNumbers=false').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.every(p => !_.has(p, 'roundList'))).toBe(true);
    });

    it('should not include win times by default', async () => {
        const res = await request(app.app).get('/leaderboard').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.every(p => p.winTimeList === undefined)).toBe(true);
    });

    it('should include win times if includeWinTimes=true', async () => {
        const res = await request(app.app).get('/leaderboard?includeWinTimes=true').expect(200);
        const body = res.body as LeaderboardResponse;
        expect(body.leaderboard.every(p => _.isArray(p.winTimeList))).toBe(true);
    });

    describe('players + ranks params', () => {
        it('should only include specified players', async () => {
            const res = await request(app.app).get('/leaderboard?players=provium,provium3').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium', 'Provium3']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 3]);
        });

        it('should only include specified ranks', async () => {
            const res = await request(app.app).get('/leaderboard?ranks=1,3').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium', 'Provium3']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 3]);
        });

        it('should include both specified ranks and players', async () => {
            const res = await request(app.app).get('/leaderboard?players=provium&ranks=3').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium', 'Provium3']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 3]);
        });

        it('should return invalid players', async () => {
            const res = await request(app.app).get('/leaderboard?players=notARealName').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard).toEqual([]);
            expect(body.invalidUsernames).toEqual(['notARealName']);
        });

        it('should return invalid ranks', async () => {
            const res = await request(app.app).get('/leaderboard?ranks=0,-1,4,notANumber').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard).toEqual([]);
            expect(body.invalidRanks).toEqual(['0', '-1', '4', 'notANumber']);
        });

        it('should respect rank range filter', async () => {
            const res = await request(app.app)
                .get('/leaderboard?fromRank=2&count=1&players=provium,provium2')
                .expect(200);
            const body = res.body as LeaderboardResponse;

            // Only returns provium2, provium is outside rank range
            expect(body.leaderboard.length).toBe(1);
            expect(body.leaderboard[0].rank).toBe(2);
            expect(body.leaderboard[0].username).toBe('Provium2');

            expect(body.invalidUsernames).toEqual(['provium']);
        });

        it('should work if array notation is used', async () => {
            const res = await request(app.app).get('/leaderboard?ranks[]=1&ranks[]=2').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 2]);
        });
    });

    describe('round filters', () => {
        it('should filter with fromRound', async () => {
            const res = await request(app.app).get('/leaderboard?fromRound=4').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium2', 'Provium3']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 2]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([3, 2]);
        });

        it('should filter with toRound', async () => {
            const res = await request(app.app).get('/leaderboard?toRound=3').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([3]);
        });

        it('should filter with fromRound + toRound', async () => {
            const res = await request(app.app).get('/leaderboard?fromRound=3&toRound=5').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium2', 'Provium']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 2]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([2, 1]);
        });

        it('should filter with fromTime', async () => {
            const res = await request(app.app).get('/leaderboard?fromTime=2018-01-03').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium3']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([2]);
        });

        it('should filter with toTime', async () => {
            const res = await request(app.app).get('/leaderboard?toTime=2018-01-03').expect(200);
            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium', 'Provium2']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 2]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([3, 3]);
        });

        it('should filter with fromTime + toTime', async () => {
            const res = await request(app.app)
                .get('/leaderboard?fromTime=2018-01-02&toTime=2018-01-03')
                .expect(200);

            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium2', 'Provium']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1, 2]);
            expect(body.leaderboard.map(p => p.numWins)).toEqual([3, 1]);
        });

        it('should intersect if round and time filter are both provided', async () => {
            const res = await request(app.app)
                .get('/leaderboard?fromTime=2018-01-02T03:00:00&toRound=5')
                .expect(200);

            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard.map(p => p.username)).toEqual(['Provium2']);
            expect(body.leaderboard.map(p => p.rank)).toEqual([1]);
            expect(body.leaderboard.map(p => p.roundList!.length)).toEqual([2]);
        });

        it('should support the filter language', async () => {
            const res = await request(app.app)
                .get('/leaderboard?filterRounds=roundNumber gte 4')
                .expect(200);

            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard).toEqual([
                { username: 'Provium2', rank: 1, roundList: [4, 5, 6], numWins: 3 },
                { username: 'Provium3', rank: 2, roundList: [7, 8], numWins: 2 },
            ]);
        });

        it('should support OR in the filter language', async () => {
            const res = await request(app.app)
                .get('/leaderboard?filterRounds=roundNumber gt 7 or roundNumber lt 3')
                .expect(200);

            const body = res.body as LeaderboardResponse;
            expect(body.leaderboard).toEqual([
                { username: 'Provium', rank: 1, roundList: [1, 2], numWins: 2 },
                { username: 'Provium3', rank: 2, roundList: [8], numWins: 1 },
            ]);
        });
    });

    describe('leaderboard count', () => {
        it('should return the number of winners', async () => {
            const res = await request(app.app).get('/leaderboard/count').expect(200);
            expect(res.body.numWinners).toBe(3);
        });

        it('should filter with fromRound', async () => {
            const res = await request(app.app).get('/leaderboard/count?fromRound=4').expect(200);
            expect(res.body.numWinners).toBe(2);
        });

        it('should filter with toRound', async () => {
            const res = await request(app.app).get('/leaderboard/count?toRound=3').expect(200);
            expect(res.body.numWinners).toBe(1);
        });

        it('should filter with fromRound + toRound', async () => {
            const res = await request(app.app).get('/leaderboard/count?fromRound=4&toRound=6').expect(200);
            expect(res.body.numWinners).toBe(1);
        });

        it('should filter with fromTime', async () => {
            const res = await request(app.app).get('/leaderboard/count?fromTime=2018-01-03').expect(200);
            expect(res.body.numWinners).toBe(1);
        });

        it('should filter with toTime', async () => {
            const res = await request(app.app).get('/leaderboard/count?toTime=2018-01-03').expect(200);
            expect(res.body.numWinners).toBe(2);
        });

        it('should filter with fromTime + toTime', async () => {
            const res = await request(app.app)
                .get('/leaderboard/count?fromTime=2018-01-02T03:00:00&toTime=2018-01-03')
                .expect(200);

            expect(res.body.numWinners).toBe(1);
        });

        it('should filter on end time', async () => {
            const res = await request(app.app).get('/leaderboard/count?fromTime=2018-01-02').expect(200);
            expect(res.body.numWinners).toBe(3);
        });

        it('should intersect if round and time filter are both provided', async () => {
            const res = await request(app.app)
                .get('/leaderboard/count?fromTime=2018-01-02T03:00:00&toRound=5')
                .expect(200);

            expect(res.body.numWinners).toBe(1);
        });

        it('should support the filter language', async () => {
            const res = await request(app.app)
                .get('/leaderboard/count?filterRounds=roundNumber gte 4')
                .expect(200);

            expect(res.body.numWinners).toBe(2);
        });

        it('should support OR in the filter language', async () => {
            const res = await request(app.app)
                .get('/leaderboard/count?filterRounds=roundNumber gt 7 or roundNumber lt 3')
                .expect(200);

            expect(res.body.numWinners).toBe(2);
        });
    });
});
