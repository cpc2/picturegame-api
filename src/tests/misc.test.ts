import * as request from 'supertest';

import * as utils from './testutils';

describe('miscellaneous integration tests', () => {
    it('should return 500 if an unexpected error is thrown', async () => {
        const app = await utils.getTestServer({ test: { throwOnRequest: true } });

        const res = await request(app.app).get('/current').expect(500);
        expect(res.body).toEqual({
            message: 'Something went wrong. Please try again or contact Provium if the issue persists.',
        });

        await app.teardown();
    });

    it('should redirect / to /docs', async () => {
        const app = await utils.getTestServer();
        const res = await request(app.app).get('/').expect(302);
        expect(res.header.location).toBe('/docs');
        await app.teardown();
    });
});
