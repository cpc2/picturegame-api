import * as express from 'express';
import * as winston from 'winston';

export interface Config {
    api: ApiConfig;
    test?: TestConfig;
}

export interface ApiConfig {
    port: number;
    authBypass: boolean;
    logging: LogConfig;
    pageSizeLimit: number;
}

export interface LogConfig {
    level: string;
    console: boolean;
}

export interface TestConfig {
    throwOnRequest?: boolean;
}

/**
 * Like the built-in Partial<T> type, but recursively makes all properties of sub-objects optional as well
 */
export type RecursivePartial<T> = {
    [K in keyof T]?: RecursivePartial<T[K]>;
}

export interface Response extends express.Response {
    locals: {
        id: string;
        startTime: number;
        logger: winston.Logger;
    };
}
