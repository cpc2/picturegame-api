import { Logger } from 'winston';

import { DbWrapper } from '../db';

export class BaseController {
    constructor(protected db: DbWrapper, protected logger: Logger) {
    }
}
