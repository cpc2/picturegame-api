import * as _ from 'lodash';

import { BaseController } from './BaseController';

import { PlayerMetricsFilters, RoundAggregatesFilters, RoundFilters } from '../db/RoundsTable';
import { BadRequestError, NotFoundError } from '../models/Error';
import * as model from '../models/model';
import { RoundNumberValidator, RoundValidator } from '../validation';

export class RoundsController extends BaseController {
    async getRoundForNumber(rawRoundNumber: string) {
        const validator = new RoundNumberValidator();
        const roundNumber = validator.validateAndParse(rawRoundNumber);

        const round = await this.db.rounds.getSpecificRound(roundNumber);
        if (!round) {
            throw new NotFoundError(NotFoundError.RoundNotFound);
        }

        return round;
    }

    async getRounds(filters: RoundFilters): Promise<model.ListResponse<model.Round>> {
        const result = await this.db.rounds.getRounds(filters);

        this.logger.info('Got rounds list', { numRounds: result.count });

        return {
            totalNumResults: result.count,
            startIndex: filters.offset ?? 0,
            pageSize: filters.limit,
            results: result.results,
        };
    }

    async postRound(rawRound: model.Round): Promise<model.UpdateRoundResponse> {
        if (!rawRound || !_.isObject(rawRound) || _.isArray(rawRound) || _.isEmpty(rawRound)) {
            throw new BadRequestError(BadRequestError.EmptyBodyError);
        }

        const validator = new RoundValidator();
        const newRound = validator.validateAndParse(rawRound);

        const oldRound = await this.db.rounds.getSpecificRound(newRound.roundNumber);
        if (oldRound) {
            throw new BadRequestError(BadRequestError.RoundAlreadyExists);
        }

        await this.db.rounds.createRound(newRound);

        const response: model.UpdateRoundResponse = { round: newRound };
        if (newRound.winnerName) {
            response.newWinner = await this.getWinnerStats(newRound.winnerName);
        }

        return response;
    }

    async patchRound(rawRoundNumber: string, rawRound: model.Round): Promise<model.UpdateRoundResponse> {
        if (!rawRound || !_.isObject(rawRound) || _.isArray(rawRound) || _.isEmpty(rawRound)) {
            throw new BadRequestError(BadRequestError.EmptyBodyError);
        }

        const numberValidator = new RoundNumberValidator();
        const roundNumber = numberValidator.validateAndParse(rawRoundNumber);

        const existingRound = await this.db.rounds.getSpecificRound(roundNumber);
        if (!existingRound) {
            throw new NotFoundError(NotFoundError.RoundNotFound);
        }

        // Don't patch the round number into the DB
        const rawUpdatedRound = Object.assign({}, existingRound, _.omit(rawRound, 'roundNumber'));

        const roundValidator = new RoundValidator();
        const updatedRound = roundValidator.validateAndParse(rawUpdatedRound);

        await this.db.rounds.updateRound(updatedRound);

        const response: model.UpdateRoundResponse = { round: updatedRound };

        const winnerChanged = existingRound.winnerName !== updatedRound.winnerName;
        if (existingRound.winnerName && winnerChanged) {
            response.oldWinner = await this.getWinnerStats(existingRound.winnerName);
        }
        if (updatedRound.winnerName && winnerChanged) {
            response.newWinner = await this.getWinnerStats(updatedRound.winnerName);
        }

        return response;
    }

    async deleteRound(rawRoundNumber: string): Promise<model.UpdateRoundResponse> {
        const validator = new RoundNumberValidator();
        const roundNumber = validator.validateAndParse(rawRoundNumber);

        const round = await this.db.rounds.deleteRound(roundNumber);

        const response: model.UpdateRoundResponse = { round };
        if (round.winnerName) {
            response.oldWinner = await this.getWinnerStats(round.winnerName);
        }

        return response;
    }

    async getWinnerStats(username: string): Promise<model.LeaderboardEntry> {
        const roundList = [...(await this.db.rounds.getRoundsForWinner(username))].sort((a, b) => a - b);
        return {
            username,
            numWins: roundList.length,
            roundList,
        };
    }

    async getRoundAggregates(input: RoundAggregatesFilters): Promise<model.ListResponse<model.RoundGroup>> {
        const result = await this.db.rounds.getRoundAggregates(input);

        this.logger.info('Got round aggregates list', { numGroups: result.count });

        return {
            totalNumResults: result.count,
            startIndex: input.offset ?? 0,
            pageSize: input.limit,
            results: result.results,
        };
    }

    async getPlayerMetrics(input: PlayerMetricsFilters): Promise<model.ListResponse<model.Player>> {
        const result = await this.db.rounds.getPlayerMetrics(input);

        this.logger.info('Got player metrics list', { numPlayers: result.length });

        return {
            totalNumResults: result.length,
            startIndex: 0,
            pageSize: result.length,
            results: result,
        };
    }
}
