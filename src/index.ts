import * as fs from 'fs';
import * as path from 'path';

import { Application } from './application';

import { createUser } from './scripts/CreateUser';

import { Config } from './index.d';

const env_ConfigFilePath = 'CONFIG_FILE';
const env_DbFilePath = 'DB_FILE';

const [method, ...args] = process.argv.slice(2);

function getConfig(filename: string): Config {
    if (!fs.existsSync(filename)) {
        throw new Error(`Config file "${filename}" not found`);
    }

    const data = fs.readFileSync(filename, 'utf8');
    return JSON.parse(data);
}

const config = getConfig(process.env[env_ConfigFilePath] ?? path.join(__dirname, '..', 'config.json'));
Application.setup(config, process.env[env_DbFilePath]).then(app => {
    switch (method) {
        case 'createuser':
            const [username, password] = args;
            return createUser(app, username, password);
        default:
            return app.start();
    }
});
