import * as Knex from 'knex';
import * as winston from 'winston';

import { Meta, MetaFields, TableName } from './model';

export abstract class UpdateableTable {
    constructor(protected db: Knex, private tableName: TableName, protected logger?: winston.Logger) {
    }

    protected async getVersion(): Promise<number> {
        const logger = this.logger;
        const versionInfo = (await this.db(TableName.Meta).where(MetaFields.TableName, this.tableName))[0];
        if (!versionInfo) {
            logger?.info('No metadata for table', { tableName: this.tableName });

            const newVersionInfo: Meta = {
                [MetaFields.TableName]: this.tableName,
                [MetaFields.Version]: 0,
            };
            await this.db(TableName.Meta).insert(newVersionInfo);
            return 0;
        }

        const version = (versionInfo as Meta)[MetaFields.Version];
        logger?.info('Table exists at version', { tableName: this.tableName, version });
        return version;
    }

    protected async setVersion(version: number): Promise<void> {
        const logger = this.logger;
        const newVersionInfo: Partial<Meta> = {
            [MetaFields.Version]: version,
        };
        await this.db(TableName.Meta).update(newVersionInfo).where(MetaFields.TableName, this.tableName);
        logger?.info('Table version set', { tableName: this.tableName, version });
    }
}
