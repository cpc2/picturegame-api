import * as bcrypt from 'bcrypt';

import * as utils from '../../tests/testutils';

import { BadRequestError, UnauthorizedError } from '../../models/Error';

import { DbWrapper } from '../DbWrapper';
import { User } from '../model';

describe('Users table unit tests', () => {
    let db: DbWrapper;
    let hash: string;

    beforeAll(async () => {
        const salt = await bcrypt.genSalt();
        hash = await bcrypt.hash('Password', salt);

        db = await utils.getTestDb({},
            `INSERT INTO users(username, password_hash) VALUES ("Provium", "${hash}");`,
        );
    });

    describe('addUser', () => {
        it('should prevent duplicate users', async () => {
            const fn = () => db.users.addUser('Provium', 'dfg');
            await utils.expectToThrowError(fn, new BadRequestError(BadRequestError.UserAlreadyExists));

            const fnCaseInsensitive = () => db.users.addUser('provium', 'dfg');
            await utils.expectToThrowError(fnCaseInsensitive, new BadRequestError(BadRequestError.UserAlreadyExists));
        });

        it('should hash passwords', async () => {
            await db.users.addUser('NewUser', 'NewPassword');
            const user = await db.users.getUser('NewUser');

            expect(user).not.toBeNull();
            expect(user!.password_hash).not.toBe('NewPassword');
            expect(await bcrypt.compare('NewPassword', user!.password_hash)).toBeTruthy();
        });
    });

    describe('getUser', () => {
        it('should return null for non-existent users', async () => {
            const user = await db.users.getUser('BadName');
            expect(user).toBeNull();
        });

        it('should return the user with their hashed password', async () => {
            const user = await db.users.getUser('Provium');
            expect(user).not.toBeNull();
            expect(user!.username).toBe('Provium');
            expect(user!.password_hash).toBe(hash);
        });
    });

    describe('validate', () => {
        it('should fail if the user is not found', async () => {
            const fn = () => db.users.validateUser('BadName', 'abc');
            utils.expectToThrowError(fn, new UnauthorizedError(UnauthorizedError.InvalidLogin));
        });

        it('should fail if the password is wrong', async () => {
            const fn = () => db.users.validateUser('Provium', 'WrongPassword');
            utils.expectToThrowError(fn, new UnauthorizedError(UnauthorizedError.InvalidLogin));

            const fnCaseSensitive = () => db.users.validateUser('Provium', 'password');
            utils.expectToThrowError(fnCaseSensitive, new UnauthorizedError(UnauthorizedError.InvalidLogin));
        });

        it('should succeed if the password is right', async () => {
            const result = await db.users.validateUser('Provium', 'Password');
            expect(result).toBeTruthy();
            expect((result as User).username).toBe('Provium');
        });
    });
});
