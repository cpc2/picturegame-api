import * as bcrypt from 'bcrypt';
import * as Knex from 'knex';
import * as winston from 'winston';

import { BadRequestError, UnauthorizedError } from '../models/Error';

import { TableName, User, UserFields } from './model';

export type Permission = 'WRITE';

export class UsersTable {
    constructor(private db: Knex, private logger?: winston.Logger) {
    }

    public async createTable() {
        const logger = this.logger;
        logger?.info('Creating users table + indexes');

        const exists = await this.db.schema.hasTable(TableName.Users);
        if (exists) {
            logger?.info('Users table already exists');
            return;
        }

        await this.db.schema.createTable(TableName.Users, table => {
            table.specificType(UserFields.Username, 'TEXT NOT NULL UNIQUE COLLATE NOCASE');
            table.text(UserFields.PasswordHash).notNullable();
            table.index([UserFields.Username], 'users_by_name');
        });

        logger?.info('Users table and indexes created successfully');
    }

    async addUser(username: string, plainPassword: string) {
        const existingUser = await this.getUser(username);

        if (existingUser) {
            throw new BadRequestError(BadRequestError.UserAlreadyExists);
        }

        const salt = await bcrypt.genSalt();
        const hash = await bcrypt.hash(plainPassword, salt);

        const newUser = {
            [UserFields.Username]: username,
            [UserFields.PasswordHash]: hash,
        };

        await this.db.insert(newUser).into(TableName.Users);
    }

    async getUser(username: string): Promise<User | null> {
        const users = await this.db(TableName.Users).where(UserFields.Username, username) as User[];
        return users.length === 1 ? users[0] : null;
    }

    async validateUser(username: string, password: string) {
        const user = await this.getUser(username);

        if (!user) {
            throw new UnauthorizedError(UnauthorizedError.InvalidLogin);
        }

        const valid = await bcrypt.compare(password, user.password_hash);

        if (!valid) {
            throw new UnauthorizedError(UnauthorizedError.InvalidLogin);
        }

        return user;
    }
}
