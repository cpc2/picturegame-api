import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as fs from 'fs';
import { Server } from 'http';
import * as Passport from 'passport';
import { BasicStrategy } from 'passport-http';
import * as path from 'path';
import * as winston from 'winston';
import * as Transport from 'winston-transport';

import { DbWrapper } from './db';
import * as endpoints from './endpoints';
import { ApiError } from './models/Error';

import { Config, LogConfig, RecursivePartial, Response } from './index.d';

function getVersion(): string {
    const filename = path.join(__dirname, '..', 'VERSION');
    if (!fs.existsSync(filename)) {
        return '99.99.99-dev';
    }
    return fs.readFileSync(filename, 'utf8').trim();
}

export class Application {
    private static _running: boolean = false;

    public app: express.Express;
    public db: DbWrapper;

    private _server: Server | null;
    private _config: Config;
    private _logger: winston.Logger;

    public static async setup(config: RecursivePartial<Config>, db?: DbWrapper | string) {
        if (Application._running) {
            throw 'Application already running!';
        }

        const application = new Application();
        application._config = config as Config;
        const logger = application._logger = configureLogging(application._config.api.logging);
        logger.info('Config loaded successfully');

        application.configureAuth();
        application.db = typeof db === 'object' ? db : await DbWrapper.setup(getVersion(), db, logger);

        application.app = express();
        application.app.enable('trust proxy');
        application.app.use(application.configureRouter());

        process.on('SIGINT', () => {
            logger.info('Caught SIGINT, shutting down...');
            application.teardown().then(() => {
                logger.info('Application successfully shut down');
                process.exit();
            });
        });

        return application;
    }

    start() {
        const port = this._config.api.port;
        this._server = this.app.listen(port, () => {
            this._logger.info(`PG API listening on port ${port}`);
            Application._running = true;
        });
    }

    private configureRouter(): express.Router {
        const router = express.Router();

        router.use(endpoints.assignLogger(this._logger));

        router.use(bodyParser.urlencoded({ extended: true }));
        router.use(bodyParser.json({ strict: false }));

        router.use((_req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
            next();
        });

        router.use('/bulk', endpoints.configureBulk(this._config, this.db));
        router.use('/rounds', endpoints.configureRounds(this._config, this.db));
        router.use('/current', endpoints.configureCurrent(this._config, this.db));
        router.use('/leaderboard', endpoints.configureLeaderboard(this._config, this.db));
        router.use('/players', endpoints.configurePlayers(this._config, this.db));
        router.use('/status', endpoints.configureStatus(this._config, this.db));
        router.use('/view', endpoints.configureViewRounds(this._config, this.db));

        router.use('/docs', express.static(path.join(__dirname, '..', '/docs')));
        router.get('/', (_req, res) => res.redirect('/docs'));

        router.use((err: any, _req: express.Request, res: Response, next: express.NextFunction) => {
            if (err instanceof ApiError) {
                res.status(err.statusCode).json(err);
            } else if (typeof err.status === 'number' && err.message) {
                res.status(err.status).json({
                    message: err.message,
                    statusCode: err.status,
                });
            } else {
                const metadata = { ...err };
                if (err instanceof Error) {
                    metadata.stack = err.stack;
                    metadata.errorMessage = err.message;
                    metadata.errorName = err.name;
                }
                res.locals.logger.error('Error processing request', metadata);
                res.status(500).json({
                    message: 'Something went wrong. Please try again or contact Provium if the issue persists.',
                });
            }
            next();
        });

        router.use(endpoints.logRequestEnd);

        return router;
    }

    private configureAuth() {
        Passport.use(new BasicStrategy((username, password, done) => {
            this.db.users.validateUser(username, password)
                .then(user => done(null, user))
                .catch(e => done(e));
        }));
    }

    public async teardown() {
        await this.db?.close();
        await this.closeServer();
        Application._running = false;
    }

    private closeServer() {
        return new Promise<void>(res => {
            !this._server ? res() : this._server.close(() => {
                this._server = null;
                res();
            });
        });
    }
}

export function configureLogging(config: LogConfig) {
    const transports: Transport[] = [];

    if (config.console || !transports.length) {
        // Ensure we have at least one transport so that winston doesn't spam console.error
        transports.push(new winston.transports.Console({
            format: winston.format.combine(
                winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                winston.format.printf(info => {
                    const { timestamp, level, message, ...data } = info;
                    return `[${timestamp}] ${level}: ${message} ${JSON.stringify(data)}`;
                })),
            silent: !config.console,
        }));
    }

    return winston.createLogger({
        level: config.level,
        exitOnError: false,
        transports,
    });
}
