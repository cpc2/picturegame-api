import * as _ from 'lodash';

import { ValidationErrorCode } from '../models/Error';
import { Round } from '../models/model';

import { coerceToInt, Validator } from './Validator';

export const validUsername = /^[a-zA-Z0-9-_]{1,20}$/;

export class RoundNumberValidator extends Validator<string, number> {
    validate(roundNumber: string) {
        this.validatePositiveInteger(roundNumber, 'roundNumber', true);
    }

    parse(roundNumber: string): number {
        return coerceToInt(roundNumber) as number;
    }
}

export class RoundValidator extends Validator<Round, Round> {
    validate(round: Round) {
        this._errors = [];

        this.validatePositiveInteger(round.roundNumber, 'roundNumber', true);

        this.validateUsername(round.hostName);
        this.validateUsername(round.winnerName);

        this.validateTimeWindow(round);

        if (_.isString(round.hostName) && _.isString(round.winnerName) && round.hostName === round.winnerName) {
            this._errors.push(ValidationErrorCode.RoundCannotBeWonByHost);
        }
    }

    parse(round: Round): Round {
        return {
            roundNumber: coerceToInt(round.roundNumber) as number,
            title: round.title,
            postUrl: round.postUrl,
            thumbnailUrl: round.thumbnailUrl,
            id: round.id,
            winningCommentId: round.winningCommentId,
            hostName: round.hostName,
            postTime: coerceToInt(round.postTime),
            winnerName: round.winnerName,
            winTime: coerceToInt(round.winTime),
            plusCorrectTime: coerceToInt(round.plusCorrectTime),
            abandonedTime: coerceToInt(round.abandonedTime),
        };
    }

    private validateUsername(username: any) {
        if (!_.isNil(username) && (!_.isString(username) || !username.match(validUsername))) {
            this._errors.push(ValidationErrorCode.UsernameMustBeValid);
        }
    }

    private validateTimeWindow(round: Round) {
        this.validatePositiveInteger(round.postTime, 'postTime');
        this.validatePositiveInteger(round.winTime, 'winTime');
        this.validatePositiveInteger(round.plusCorrectTime, 'plusCorrectTime');
        this.validatePositiveInteger(round.abandonedTime, 'abandonedTime');

        const start = coerceToInt(round.postTime);
        const end = coerceToInt(round.winTime);
        const correct = coerceToInt(round.plusCorrectTime);
        const abandoned = coerceToInt(round.abandonedTime);

        if (_.isNumber(start) && _.isNumber(end) && start >= end) {
            this._errors.push(ValidationErrorCode.RoundMustEndAfterItBegins);
        }

        if (_.isNumber(start) && _.isNumber(correct) && start >= correct) {
            this._errors.push(ValidationErrorCode.RoundCannotBeCorrectedBeforeStart);
        }

        if (_.isNumber(correct) && _.isNumber(end) && end > correct) {
            this._errors.push(ValidationErrorCode.RoundCannotBeCorrectedBeforeAnswer);
        }

        if (_.isNumber(abandoned) && _.isNumber(start) && start >= abandoned) {
            this._errors.push(ValidationErrorCode.RoundCannotBeAbandonedBeforeItStarts);
        }
    }

}
