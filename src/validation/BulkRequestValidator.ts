import { Config } from '../index.d';
import { ValidationErrorCode } from '../models/Error';
import { BulkRequest, SingleLeaderboardRequest, SingleRoundsAggregateRequest, SingleRoundsRequest } from '../models/model';

import { LeaderboardFilters, RoundAggregatesFilters, RoundFilters } from '../db/RoundsTable';

import { coerceToBoolean, coerceToInt, Validator } from './Validator';

const maxRequestsPerBulk = 30;

export class BulkRequestValidator extends Validator<BulkRequest, BulkRequest> {
    validate(req: BulkRequest) {
        this.validateArray(req.requests, 'requests', true);
        if (this._errors.length) {
            return;
        }

        if (req.requests.length > maxRequestsPerBulk || req.requests.length < 1) {
            this._errors.push(ValidationErrorCode.BulkRequestHasInvalidNumRequests(maxRequestsPerBulk));
        }
    }

    parse(req: BulkRequest) { return req; }
}

export class SingleLeaderboardRequestValidator extends Validator<SingleLeaderboardRequest, LeaderboardFilters> {
    validate(req: SingleLeaderboardRequest): void {
        this.validatePositiveInteger(req.fromRank, 'fromRank');
        this.validatePositiveInteger(req.count, 'count');

        // The actual contents of the arrays are validated in the controller; the rest of the request can complete if some are invalid
        this.validateArray(req.players, 'players');
        this.validateArray(req.ranks, 'ranks');

        this.validateBoolean(req.includeRoundNumbers, 'includeRoundNumbers');
        this.validateBoolean(req.includeWinTimes, 'includeWinTimes');

        this.validateString(req.filterRounds, 'filterRounds');
    }

    parse(req: SingleLeaderboardRequest): LeaderboardFilters {
        return {
            fromRank: coerceToInt(req.fromRank),
            count: coerceToInt(req.count),
            players: req.players,
            ranks: req.ranks,
            includeRoundNumbers: coerceToBoolean(req.includeRoundNumbers) ?? true,
            includeWinTimes: coerceToBoolean(req.includeWinTimes),
            filterRounds: req.filterRounds,
        };
    }
}

export class SingleRoundsAggregateRequestValidator extends Validator<SingleRoundsAggregateRequest, RoundAggregatesFilters> {
    constructor(private config: Config) {
        super();
    }

    validate(req: SingleRoundsAggregateRequest): void {
        this.validateString(req.filter, 'filter');
        this.validateArray(req.select, 'select');
        this.validateArray(req.sort, 'sort');

        this.validateString(req.groupBy, 'groupBy');
        this.validateString(req.groupFilter, 'groupFilter');

        this.validatePositiveInteger(req.limit, 'limit');
        this.validateNonNegativeInteger(req.offset, 'offset');
    }

    parse(req: SingleRoundsAggregateRequest): RoundAggregatesFilters {
        return {
            filter: req.filter,
            select: req.select,
            sort: req.sort,
            groupBy: req.groupBy,
            groupFilter: req.groupFilter,
            limit: Math.min(coerceToInt(req.limit) ?? this.config.api.pageSizeLimit, this.config.api.pageSizeLimit),
            offset: coerceToInt(req.offset),
        };
    }
}

export class SingleRoundsRequestValidator extends Validator<SingleRoundsRequest, RoundFilters> {
    constructor(private config: Config) {
        super();
    }

    validate(req: SingleRoundsRequest): void {
        this.validateString(req.filter, 'filter');
        this.validateArray(req.select, 'select');
        this.validateArray(req.sort, 'sort');

        this.validatePositiveInteger(req.limit, 'limit');
        this.validateNonNegativeInteger(req.offset, 'offset');
    }

    parse(req: SingleRoundsRequest): RoundFilters {
        return {
            filter: req.filter,
            select: req.select,
            sort: req.sort,
            limit: Math.min(coerceToInt(req.limit) ?? this.config.api.pageSizeLimit, this.config.api.pageSizeLimit),
            offset: coerceToInt(req.offset),
        };
    }
}
