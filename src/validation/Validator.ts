import * as _ from 'lodash';
import * as moment from 'moment';

import { BadRequestError, ValidationErrorCode } from '../models/Error';

export const validPositiveInteger = /^\d+$/;

export abstract class Validator<S, T = S> {
    protected _errors: string[];

    constructor() {
        this._errors = [];
    }

    validateAndParse(item: S): T {
        this.validate(item);
        if (this._errors.length > 0) {
            throw new BadRequestError(BadRequestError.ValidationFailed, { errors: this._errors });
        }
        return this.parse(item);
    }

    abstract validate(item: S): void;
    abstract parse(item: S): T;

    get errors() {
        return [...this._errors];
    }

    protected validateNonNegativeInteger(num: any, fieldName: string, required: boolean = false) {
        this.validateInteger(num, ValidationErrorCode.ValueMustBeNonNegativeInteger(fieldName), required, 0);
    }

    protected validatePositiveInteger(num: any, fieldName: string, required: boolean = false) {
        this.validateInteger(num, ValidationErrorCode.ValueMustBePositiveInteger(fieldName), required, 1);
    }

    private validateInteger(num: any, errorText: string, required: boolean, minValue: number) {
        if (_.isNil(num)) {
            if (required) {
                this._errors.push(errorText);
            }
            return;
        }
        if (_.isString(num)) {
            if (num.match(validPositiveInteger)) {
                return;
            }
            this._errors.push(errorText);
            return;
        }
        if (!_.isInteger(num) || num < minValue) {
            this._errors.push(errorText);
        }
    }

    protected validateDateTime(value: any, fieldName: string, required: boolean = false) {
        if (_.isNil(value)) {
            if (required) {
                this._errors.push(ValidationErrorCode.ValueMustBeValidDateTime(fieldName));
            }
            return;
        }

        if (!_.isString(value)) {
            this._errors.push(ValidationErrorCode.ValueMustBeValidDateTime(fieldName));
        }

        const datetime = moment.utc(value, moment.ISO_8601, true);
        if (!moment.isMoment(datetime) || !datetime.isValid()) {
            this._errors.push(ValidationErrorCode.ValueMustBeValidDateTime(fieldName));
        }
    }

    protected validateString(value: any, fieldName: string, required: boolean = false) {
        if (_.isNil(value)) {
            if (required) {
                this._errors.push(ValidationErrorCode.ValueMustBeString(fieldName));
            }
        } else if (!_.isString(value)) {
            this._errors.push(ValidationErrorCode.ValueMustBeString(fieldName));
        }
    }

    protected validateBoolean(value: any, fieldName: string, required: boolean = false) {
        if (_.isNil(value)) {
            if (required) {
                this._errors.push(ValidationErrorCode.ValueMustBeBoolean(fieldName));
            }
        } else if (_.isString(value)) {
            if (value !== 'true' && value !== 'false') {
                this._errors.push(ValidationErrorCode.ValueMustBeBoolean(fieldName));
            }
        } else if (!_.isBoolean(value)) {
            this._errors.push(ValidationErrorCode.ValueMustBeBoolean(fieldName));
        }
    }

    protected validateArray(value: any, fieldName: string, required: boolean = false) {
        if (_.isNil(value)) {
            if (required) {
                this._errors.push(ValidationErrorCode.ValueMustBeArray(fieldName));
            }
        } else if (!_.isArray(value)) {
            this._errors.push(ValidationErrorCode.ValueMustBeArray(fieldName));
        }
    }
}

export function coerceToInt(val?: string | number) {
    if (_.isNil(val)) {
        return val;
    }
    return _.isString(val) ? parseInt(val, 10) : val;
}

export function coerceToBoolean(val?: string | boolean) {
    if (_.isNil(val)) {
        return val;
    }

    return _.isString(val) ? (val === 'true') : val;
}

export function coerceToDate(val?: string) {
    if (_.isNil(val)) {
        return val;
    }

    return moment.utc(val, moment.ISO_8601, true);
}
