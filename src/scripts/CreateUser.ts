import { Application } from '../application';

export async function createUser(app: Application, username: string, password: string) {
    if (!username || !password) {
        console.error('Usage: node index.js createuser <username> <password>');
        return await app.teardown();
    }
    try {
        await app.db.users.addUser(username, password);
    } catch (e) {
        console.error('Error creating user:', e);
    } finally {
        await app.teardown();
    }
}
